const DB = require('nedb-promise')

const {secret} = require('../../config.js')
const {derive, verify} = require('./'+secret.scheme)(secret)

const db = {
	s: DB({filename: 'data/s.db', autoload: true }),
}
db.s.ensureIndex({fieldName: 'k',	unique: true})

module.exports = {
	create(k, v){
		return derive(v)
			.then(s => db.s.insert({k, s}))
			.then() // return nothing
	},
	verify(k, claim){
		const s = db.s.findOne({k})
		return Promise.all([
			s,
			s.then(
				o => !!o && verify(o.s, claim)
			),
		]).then(([s, valid]) => ({
			exists: !!s,
			valid,
		}))
	},
}
