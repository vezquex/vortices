const MAX_TIME = 0.1 // s
const {promisify} = require('util')
const scrypt = require('scrypt')
const kdf = promisify(scrypt.kdf)
const verify = promisify(scrypt.verifyKdf)

module.exports = ({max_time=MAX_TIME}) => {
	const params = scrypt.paramsSync(max_time)
	return {
		derive: s => kdf(s, params).then(b => b.toString('base64')),
		verify: (secret, claim) => verify(
			Buffer.from(secret, 'base64'),
			claim
		),
	}
}
