const crypto = require('crypto')
const {promisify} = require('util')
const pbkdf2 = promisify(crypto.pbkdf2)

module.exports = function({
	alg='sha512',
	stretch=1e5,
	len=512
}){
	const derive = (secret, salt, alg_, stretch_, len_) => {
		alg_ = alg_ || alg
		stretch_ = stretch_ || stretch
		len_ = len_ || len
		salt = salt || crypto.randomBytes(16).toString('hex')
		return pbkdf2(secret, salt, stretch_, len_, alg_)
			.then(key => [
				alg_, len_, stretch_,
				salt.toString('hex'),
				key.toString('hex')
			].join('-'))
	}
	return {
		derive,
		verify(secret, claim){
			const [alg, len, stretch, salt] = secret.split('-')
			return derive(claim, salt, alg, parseInt(stretch), parseInt(len))
				.then(claim => crypto.timingSafeEqual(
					Buffer.from(claim),
					Buffer.from(secret)
				))
		},
	}
}
