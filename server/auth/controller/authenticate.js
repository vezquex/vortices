const token = require('../../token/model')
const {auth, get, key, register} = require('../model')

const tokenRespond = (res, k) => {
	get(k).then(({name}) => {
		const t = token.create({name, k})
		res.cookie('t', t, {httpOnly: true})
		res.json({name, k, t})
	})
}

module.exports = (req, res) => {
	const {method, name, secret} = req.body || {}
	if(!name){
		return res.status(400).json('Name required.')
	}
	const k = key(name)
	if(!k){
		return res.status(400).json(`Name did not contain any allowable characters: ${name}`)
	}
	const registering = method === 'register'
	auth(k, secret)
		.then(({exists, valid}) => {
			if(!exists && !registering){
				return res.status(400).json(`User not found: ${k}`)
			}
			if(!exists && registering){
				return register(name, secret)
					.then(() => { tokenRespond(res, k) })
			}
			if(!valid && registering){
				return res.status(400).json(`User name taken: ${k}`)
			}
			if(!valid){
				return res.status(400).json(`Passphrase mismatch for ${name}`)
			}
			tokenRespond(res, k)
		})
		.catch(e => {
			res.status(400).json('Error. ' + e)
		})
}
