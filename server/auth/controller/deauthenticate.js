const token = require('../../token/model')

module.exports = (req, res) => {
	const t = req.params.t || ((req.body||{}).t) || req.cookies.t
	const all = !!(req.params.all || ((req.body||{}).all))
	if(!t){
		return res.status(400).json('Token (t) is required.')
	}
	const destruction = all ?
		token.get(t).then(
			({name}) => token.destroyBy('name', name)
		)
		: token.destroy(t)
	destruction
		.then(() => {
			res.clearCookie('t')
			res.status(200).json('Signed out.')
		})
		.catch(e => {
			res.status(400).json('Error. ' + e.message)
		})
}
