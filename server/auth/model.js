const {key} = require('../../src/util/auth')
const secret = require('../secret/model')
const now = require('../../src/util/now')
const DB = require('nedb-promise')
const db = DB({filename: 'data/u.db', autoload: true })
db.ensureIndex({fieldName: 'k',	unique: true})

const auth = (k, claim) => {
	return secret.verify(k, claim)
}

const create = ({name, published}) => {
	return db.insert({
		k: key(name),
		name,
		published: now(published),
	})
}

const get = (k) => {
	return db.findOne({k})
}

const register = (name, passphrase) => {
	return create({name})
	.then(()=>
		secret.create(key(name), passphrase)
	)
}

module.exports = {auth, get, key, register}
