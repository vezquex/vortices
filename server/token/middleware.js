const token = require('./model')

module.exports = (req, res, next) => {
	token.get(req.query.t || ((req.body||{}).t) || req.cookies.t)
	.then(o => {
		req.token = o
		next()
	})
	.catch(e => res.status(400).json({id:'invalid-token',message:'Invalid token'}))
}
