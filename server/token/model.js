const crypto = require('crypto')
const DB = require('nedb-promise')

const db = DB({filename: 'data/t.db', autoload: true })
db.ensureIndex({fieldName: 't',	unique: true})

module.exports = {
	create(o){
		const t = crypto.randomBytes(16).toString('hex')
		db.insert({...o, t})
		return t
	},
	get(t){
		return db.findOne({t})
		.then(o => o || Promise.reject({id:'invalid-token',message:'Invalid token'}))
	},
	destroy(t){
		return db.remove({t})
	},
	destroyBy(k, v){
		return db.remove({[k]:v}, {multi:true})
	},
}
