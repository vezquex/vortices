const {get, remove} = require('../model')
const {key} = require('../../auth/model')

module.exports = (req, res) => {
	const {body, token} = req
	const {k} = body
	get(k)
	.then(o => {
		if(!o) throw new Error(`Invalid item: ${k}: ${o}`)
		if(key(o.actor) !== token.k) throw new Error(`Actor mismatch: ${o.actor} !== ${token.k}`)
		remove(k)
	})
	.then(() =>
		res.status(201).json('Removed')
	)
	.catch(e =>
		res.status(400).json('Remove error: ' + e)
	)
}