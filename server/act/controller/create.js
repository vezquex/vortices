const {create, send} = require('../model')

module.exports = (req, res) => {
	const {body, token} = req
	const actor = token.name
	return create({...body, actor})
	.then(o => {
		return res.status(201).json(o)
	})
	.catch(e =>
		res.status(400).json('Create error: ' + e)
	)
}