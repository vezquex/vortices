const {get} = require('../model')

module.exports = (req, res) => {
	const {k} = req.body
	get(k)
	.then(o => o ? res.json(o) : res.status(404).json('Not Found'))
	.catch(e => res.status(404).json({e}))
}