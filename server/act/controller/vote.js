const _ = require('lodash')
const {vote} = require('../model')

const dirs = ['down', 'up']

module.exports = (req, res) => {
	const {body, token} = req
	const {k} = body
	return vote({
		actor: token.k,
		o: k,
		vote: _.mapValues(
			_.pick(body, dirs),
			v => (v > 0) ? 1 : 0
		)
	})
	.then(([vote, get]) => res.json({...get, vote}))
	.catch(e => res.status(400).json('Vote error: ' + e))
}