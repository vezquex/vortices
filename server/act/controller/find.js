const _ = require('lodash')
const {count, find} = require('../model')

module.exports = (req, res) => {
	const {desc, limit, offset, sort} = req.body
	const filter = _.pick(req.body, ['actor', 'tags', 'to'])
	const filtering = Object.keys(filter).length ? {filter} : {}
	Promise.all([
		count(filter),
		find({
			...filtering,
			limit: parseInt(limit),
			offset: parseInt(offset),
			sort: {[sort]: desc ? -1 : 1},
		}),
	])
	.then(([count, find]) => res.json({count, find}))
	.catch(e => res.status(404).send(e))
}