const {get, updateGet} = require('../model')
const {key} = require('../../auth/model')

module.exports = (req, res) => {
	const {body, token} = req
	get(body.k)
	.then(o => {
		if(!o) throw new Error(`Invalid item: ${k}: ${o}`)
		if(key(o.actor) !== token.k) throw new Error(`Actor mismatch: ${o.actor} !== ${token.k}`)
		return updateGet(body)
	})
	.then(o => res.status(200).json(o))
	.catch(e =>	res.status(400).json(
		'Update error: ' + e
	))
}