const {uniq} = require('lodash')

const ignore = /[-_]+/g
const split = /[^a-z0-9]+/

const parse = s => {
  return s ?
    uniq(
      s.toLowerCase()
        .replace(ignore, '')
        .split(split)
    )
    : []
}

module.exports = {parse}