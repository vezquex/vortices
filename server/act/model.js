const LIMIT = 10
const LIMIT_MAX = 100
const OFFSET = 0

const _ = require('lodash')
const crypto = require('crypto')
const {pickBy} = require('lodash')
const DB = require('nedb-promise')
const Tag = require('./tag')
const dbs = require('../../config').dbs || {o:'o'}

const db = {
	o: DB({filename: `data/${dbs.o}.db`, autoload: true }),
	c: DB({filename: 'data/c.db', autoload: true }),
	v: DB({filename: 'data/v.db', autoload: true }),
}
db.o.ensureIndex({fieldName: 'k',	unique: true})

const returnUpdatedDocs = ([_, d]) => d

const types = ['Object', 'Collection']
const typeError = () => Promise.reject(`Type must be one of: ${types.join(', ')}`)
const typeInvalid = type => !types.some(t => t === type)

const model = {
	count(filter){
		return db.o.count(filter)
	},
	create({actor, bump, content, href, mediaType, name, published, svg, tags, to, type, url}){
		const k = crypto.randomBytes(16).toString('hex')
		type = type || 'Object'
		if(typeInvalid(type)){
			return typeError()
		}
		bump && to && db.o.update({k:to}, {
			$inc:{replies:1},
			$set: {updated: (new Date()).toISOString()},
		})
		published = (published ? new Date(published) : new Date()).toISOString()
		return db.o.insert({
			actor,
			content,
			down: 0,
			href,
			k,
			name,
			published,
			svg,
			tags: Tag.parse(tags),
			to,
			type,
			mediaType,
			up: 0,
			updated: published,
			url,
		})
	},
	get(k){
		return db.o.findOne({k})
	},
	find({filter, limit, offset, sort}){
		return db.o.cfind(filter)
			.sort(sort || {published: -1})
			.skip(Math.max(offset || OFFSET, 0))
			.limit(Math.min(LIMIT_MAX, Math.max(1, limit || LIMIT)))
			.exec()
	},
	prop(k, p){
		return db.o.findOne({k}, {[p]:1}).then(o => o[p])
	},
	remove(k){
		return db.o.remove({k})
	},
	set_prop(k, prop, val){
		return db.o.update({k}, {$set: {[prop]: val}})
	},
	update({actor, content, href, k, name, svg, tags, type, url}){
		type = type || 'Object'
		if(typeInvalid(type)){
			return typeError()
		}
		return db.o.update({k}, {$set: pickBy({
			actor,
			content,
			href,
			name,
			svg,
			tags: Tag.parse(tags),
			type,
			updated: (new Date()).toISOString(),
			url,
		}, v => v !== (void 0) )})
	},
	updateGet(o){
		return model.update(o).then(()=>model.get(o.k))
	},
	add(k, os){
		return db.c.insert(os.map(o => {k, o}))
	},
	send(o, ks){
		return db.c.insert(ks.map(k => {k, o}))
	},
	item_keys(k, offset, limit){
		offset = Math.max(offset || OFFSET, 0)
		limit = Math.min(LIMIT_MAX, Math.max(1, limit || LIMIT))
		return db.c.cfind({k})
			.skip(offset)
			.limit(limit)
			.then(a => a.map(({o}) => o))
	},
	items(k, offset = OFFSET, limit = LIMIT){
		return model.item_keys(k, offset, limit).then(ks => {
			return db.o.find({ k: {$or: ks} })
		})
	},
	vote({actor, o, vote}){
		return db.v.findOne({actor, o}).then(old => {
			old = old || {}
			const delta = _.mapValues(vote, (v,k) => v - (old[k]||0))
			const q = {actor, o}
			return Promise.all([
				db.v.update(q, {$set: {...q, ...vote}},
					{returnUpdatedDocs:true, upsert:true})
					.then(returnUpdatedDocs),
				db.o.update({k:o}, {$inc:{...delta}},
					{returnUpdatedDocs:true})
					.then(returnUpdatedDocs),
			])
		})
	}
}

module.exports = model
