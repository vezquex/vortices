const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const path = require('path')
const token = require('./token/middleware')
var helmet = require('helmet')

const build = path.join(__dirname, '../build')

const app = express()
const json = bodyParser.json()
 
app.use(
  helmet({
    contentSecurityPolicy: false,
  }),
  express.static(build),
  cookieParser(),
)

app.post('/api/authenticate', json, require('./auth/controller/authenticate'))
app.post('/api/deauthenticate', json, require('./auth/controller/deauthenticate'))

app.post('/api/act/find', json, require('./act/controller/find'))
app.post('/api/act/get', json, require('./act/controller/get'))
app.post('/api/act/create', json, token, require('./act/controller/create'))
app.post('/api/act/remove', json, token, require('./act/controller/remove'))
app.post('/api/act/update', json, token, require('./act/controller/update'))
app.post('/api/act/vote', json, token, require('./act/controller/vote'))

app.get('/*', function (req, res){
  res.sendFile(path.join(build, 'index.html'))
})

app.listen(4000)