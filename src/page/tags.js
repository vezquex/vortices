import {Helmet} from 'react-helmet'
import {Link} from 'react-router-dom'
import {history} from '../history'
const {Component, createElement: h, createRef} = require('react')

const tags = [
	// 'Artist',
	// 'Album',
	// 'Song',
	'Funny',
	'Image',
	'News',
	'Music',
	'Video',
]

export default class extends Component {
	constructor(props){
		super(props)
		this.input = createRef()
	}
	goTag = (ev)=>{
		ev.preventDefault()
		history.push('/tag/' + this.input.current.value)
	}
	render(){
		// const {props, state} = this
		return h('main', { className: 'main' },
			h(Helmet, null,
				h('title', null, 'Tags')
			),
			h('div', {className:'menu column'},
				h('form', {
					action:'/tag/',
					method:'get',
					onSubmit:this.goTag,
					},
					h('input', {ref:this.input, name:'tag', placeholder:'Tag', type:'search'}),
				),
				tags.map(tag => h(Link, {to:'/tag/'+tag.toLowerCase(),key:tag,className:'stripe'}, tag)),
			),
		)
	}
}