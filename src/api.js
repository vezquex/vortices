const {post} = require('./util/fetch')

const create = post('/api/act/create')

const deauthenticate = post('/api/deauthenticate')

const find = post('/api/act/find')

const get = post('/api/act/get')

const remove = post('/api/act/remove')

const update = post('/api/act/update')

const vote = post('/api/act/vote')

export default {create, deauthenticate, find, get, remove, update, vote}