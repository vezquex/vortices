import {Provider} from 'react-redux'
import {render} from 'react-dom'
import './css/main.css'
import './css/flex.css'
import store from './store'
import App from './app'
import registerServiceWorker from './registerServiceWorker'
import './css/dark/dark.css'
import './css/dark/cyan.css'
const {createElement: h} = require('react')

render(
  h(Provider, {store},
    h(App)
  ),
  document.getElementById('root')
)
registerServiceWorker()
