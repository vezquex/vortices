module.exports = (d) => (d ? new Date(d) : new Date()).toISOString()
