const post = url => o => {
  let ok
  return fetch(url, {
    body: JSON.stringify(o),
    credentials:'include',
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'post',
  })
  .then(r => {ok = r.ok; return r.json()})
  .then(r => {
    ok || console.error(r)
    return [!ok && r, ok && r]
  })
}

export {post}