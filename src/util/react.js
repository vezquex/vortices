export const checkbox = (action, key) => ev => action({[key]: ev.target.checked})
export const input = (action, key) => ev => action({[key]: ev.target.value})
