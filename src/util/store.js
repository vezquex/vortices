const store = window.localStorage

const get = k => {
  const s = store.getItem(k)
  return s && JSON.parse(s)
}
const set = (k,v) => store.setItem(k, JSON.stringify(v))
const remove = k => store.removeItem(k)

module.exports = {get, remove, set}