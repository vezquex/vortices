const {user_allowed, user_max} = require('../config')

const regExpNot = pattern => new RegExp('[^'+pattern+']', 'g')
const disallowed = regExpNot(user_allowed)

const key = (s) => {
	return s.slice(0, user_max).toLowerCase().replace(disallowed, '')
}

module.exports = {key}
