import _ from 'lodash'

export const _id = (_, x) => x
export const a_id = ([_, x]) => x
export const assign = (a, b) => ({...a, ...b})
export const assignBy = k => (target, o) => ({...target, [o[k]]:o})
export const assignAllBy = k => (target, os) => os.reduce(
  (r, o) => {r[o[k]] = o; return r},
  {...target}
)
export const id = x => x
export const omit = (o, paths) => _.omit(o, paths)
export const reject = (xs, v) => xs.filter(x => x !== v)
