import { getStore } from 'kea'
import thunkPlugin from 'kea-thunk'

export default getStore({
  plugins: [ thunkPlugin ],
})
