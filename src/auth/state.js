import api from './api'
import state from '../state'
import {assign, id} from '../util/fun'
import {kea} from 'kea'

export default kea({
  connect: {
    actions: [state, ['authenticated']],
  },
  actions: () => ({
    setData: id,
  }),
  thunks: ({ actions, get, getState }) => ({
    authenticate: o =>
      api.authenticate(o)
      .then(([e, r]) => r && actions.authenticated(r)),
  }),
  reducers: ({ actions }) => ({
    data: [{
      name: '',
    }, null, {
      [actions.setData]: assign,
    }],
  }),
})