const {post} = require('../util/fetch')

const authenticate = post('/api/authenticate')

export default {authenticate}