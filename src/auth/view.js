import {Helmet} from 'react-helmet'
import {input} from '../util/react'
import state from './state'
const {Component, createElement: h, createRef} = require('react')

export default state(class extends Component {
	constructor(props){
		super(props)
		this.secretInput = createRef()
	}
	authenticate = ev => {
		ev.preventDefault()
		this.actions.authenticate({
			method: this.props.register ? 'register' : 'authenticate',
			name: this.props.data.name,
			secret: this.secretInput.current.value,
		})
		.then(r => r && this.props.success())
	}
  render(){
		const {data, register} = this.props
		const {name} = data
		const title = register ? 'Register' : 'Sign In'
		return (
			h('div', {className:'main'},
				h(Helmet, null,
					h('title', null, title),
				),
				h('form', {
					action: '/authenticate',
					className:'form form-full',
					onSubmit: this.authenticate,
					method:'post',
					},
					h('label', {className:'label'},
						h('span', {className:'field-label'},
							'User Name'
						),
						h('input', {
							className:'valid',
							autoFocus:true,
							name:'name',
							onChange:input(this.actions.setData, 'name'),
							required:true,
							type:'text',
							value:name,
						})
					),
					h('label', {className:'label'},
						h('span', {className:'field-label'},
							'Passphrase'
						),
						h('input', {
							className:'valid',
							name:'secret',
							ref: this.secretInput,
							required:true,
							type:'password',
						})
					),
					h('button', {name:'method', className:'wide submit'},
						title
					)
				)
			)
		)
  }
})
