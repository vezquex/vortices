import {createBrowserHistory} from 'history'

const history = createBrowserHistory()

const noBackTo = ['/register', '/sign-in']
let historyLast = ['/']

const updateHistoryLast = ({pathname}) => {
  if(!noBackTo.includes(pathname)){
    historyLast = [pathname, historyLast[0]]
  }
}
updateHistoryLast(history.location)
history.listen(updateHistoryLast)

const goToRecent = () => {
  history.push(historyLast[0])
}

export {goToRecent, history}