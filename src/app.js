import Auth from './auth/view'
import config from './config'
import logo from './svg/cyclone'
import PostForm from './post/form'
import PostList from './post/list'
import queryString from 'query-string'
import state from './state'
import Tags from './page/tags'
import {Component, createElement as h} from 'react'
import {goToRecent, history} from './history'
import {NavLink, Route, Router, Switch} from 'react-router-dom'
const postTypes = {
  node: {component: require('./post/view').default},
}

const offset = location =>
  parseInt(queryString.parse(location.search).offset, 10) || 0

const find = {
  actor: (f, actor, o) => f({...o, actor}),
  replies: (f, to, o) => f({...o, to}, 'replies'),
  tags: (f, tags, o) => f({...o, tags}),
  top: (f, o) => f({...o, to:{$exists:false}, sort:'up', desc:true}),
  updated: (f, o) => f({...o, to:{$exists:false}, sort:'updated', desc:true}),
}

const renderPost = (o) =>{
  if(!o) throw new Error('renderPost')
  const {isAuth, k, nodes, offset, view} = o
  const {component, sort} = postTypes[view]
  return (component || null) && h(component, {
    find: (f, o) => find.replies(f, k, {...o, offset, sort:sort||'published'}),
    isAuth,
    item: nodes[k],
    k,
    offset,
  })
}

export default state(class extends Component {
  componentDidMount = () => {
    this.actions.preauthenticated()
  }
  deauthenticate = ev => {
    ev.preventDefault()
    this.actions.deauthenticate()
  }
  deauthenticateAll = ev => {
    ev.preventDefault()
    this.actions.deauthenticate({
      all: true,
    })
  }
  render(){
    const {props} = this
    const {auth, nodes} = props
    const {k, name} = auth
    const isAuth = !!k
    return h(Router, {history},
      h('div', {className:'app scroll'},
        h('nav', {className:'menu main-menu unvisited'},
          h(NavLink, {to:'/', exact:true, className:'shell'},
            h('span', {className:'icon-svg'}, logo),
            h('span', {className:'tab actively'}, config.title),
          ),
          h(NavLink, {to:'/top'}, 'Top'),
          h(NavLink, {to:'/tags'}, 'Tags'),
          !k && h(NavLink, {to:'/register'}, 'Register'),
          !k && h(NavLink, {to:'/sign-in', className:'shell'},
            h('span', {className:'tab actively'}, 'Sign In')
          ),
          name && h(NavLink, {to:'/u/'+name}, name),
          k && h('form', {
            action:'/api/deauthenticate',
            method:'post',
            onSubmit:this.deauthenticate,
            },
            h('button', {className:'a'}, 'Sign Out'),
          ),
          k && h(NavLink, {to:'/post', className:'shell'},
            h('span', {className:'tab actively'}, 'Post')
          ),
        ),
        h(Switch, null,
          h(Route, {exact:true, path:'/', render:({location})=>
            h(PostList, {
              className:'main',
              find:find.updated,
              list:'new',
              offset:offset(location),
              title:config.title,
            })
          }),
          h(Route, {path:'/top', render:({location})=>
            h(PostList, {
              className:'main',
              find:find.top,
              list:'top',
              offset:offset(location),
              title:'Top',
            })
          }),
          h(Route, {path:'/tag/:tag?', render:({location, match})=> {
            const tag = match.params.tag || queryString.parse(location.search).tag || ''
            return h('main', {className:'main column'},
              h('h1', null, tag),
              h(PostList, {
                className: 'expand',
                find:(f, o)=>find.tags(f, tag.toLowerCase(), o),
                list:'tag',
                offset:offset(location),
                tags:[tag],
                title:tag+' | Tag',
              })
            )
          }}),
          h(Route, {path:'/u/:u', render:({location, match})=> {
            const {u} = match.params
            return h('main', {className:'main column'},
              h('h1', null, u),
              (name === u) && h('form', {
                action:'/api/deauthenticate',
                method:'post',
                onSubmit:this.deauthenticateAll,
                },
                h('button', {name:'all',value:1,className:'a'}, 'Sign out all devices'),
              ),
              h(PostList, {
                content:true,
                find:(f, o)=>find.actor(f, u, o),
                hide:{actor:true},
                list:u,
                offset:offset(location),
                title:u+' | Profile',
              })
            )
          }}),
          h(Route, {path:'/tags', component:Tags, exact:true}),
          h(Route, {path:'/sign-in', render:()=>h(Auth, {success:goToRecent})}),
          h(Route, {path:'/register', render:()=>
            h(Auth, {register:true, success:goToRecent})}),
          k && h(Route, {path:'/post', render:()=>
            h(PostForm, {success:k=>history.push('/node/'+k), title:'Post'})}),
          k && h(Route, {path:'/edit/:k', render:r=>{
            const {k} = r.match.params
            const item = nodes[k]
            return h(PostForm, {
              item, k,
              success:k=>history.push('/node/'+k),
              title: item ? `Edit: ${item.name}` : 'Edit',
            })
          }}),
          h(Route, {path:'/node/:k',
            render:r=>renderPost({
              isAuth, k:r.match.params.k, nodes, view:'node',
              offset:offset(r.location),
            })
          }),
          h(Route, {path:'/t/:view/:k', render:r=>renderPost({
            ...r.match.params, isAuth, nodes,
          })}),
        )
      )
    )
  }
})