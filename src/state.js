import _ from 'lodash'
import api from './api'
import PropTypes from 'prop-types'
import store from './util/store'
import {a_id, _id, assignAllBy, assignBy, id, omit} from './util/fun'
import {kea} from 'kea'

const assignByK = assignBy('k')
const assignAllByK = assignAllBy('k')

export const handleError = (actions, f) => {
  return ([e,r]) => {
    console.log('handleError', e, r)
    if(e && e.id === 'invalid-token'){
      store && store.remove('u')
      actions.onAuthenticated({})
      return false
    }
    return f([e,r])
  }
}

export default kea({
  path: () => ['scenes', 'state'],
  actions: () => ({
    onAuthenticated: id,
    onCreate: a_id,
    onGet: a_id,
    onFind: a_id,
    onVote: id,
    onRemove: id,
  }),
  thunks: ({ actions, get, getState }) => ({
    authenticated: (o) => {
      const u = _.pick(o, 'k', 'name')
      store && store.set('u', u)
      actions.onAuthenticated(u)
      return true
    },
    deauthenticate: (o={}) => {
      console.log('deauthenticate', o)
      api.deauthenticate({
        all: !!o.all,
        t: getState().scenes.state.auth.t,
      })
      store && store.remove('u')
      actions.onAuthenticated({})
    },
    preauthenticated: () => {
      const u = store && store.get('u')
      u && actions.onAuthenticated(u)
    },
    get: (o) => {
      return api.get(o)
      .then(actions.onGet)
    },
    remove: (k) => {
      return api.remove({
				k,
				t: getState().scenes.state.auth.t,
			})
      .then(handleError(actions, ([e,r]) => r && actions.onRemove(k)))
    },
    vote: ({down, k, up}) => {
      return api.vote({
				down, k, up,
				t: getState().scenes.state.auth.t,
			})
      .then(handleError(actions, ([e,r]) => {
        return r && actions.onVote(r)
      }))
    },
  }),
  reducers: ({ actions }) => ({
    auth: [{u:{}}, PropTypes.object, {
      [actions.onAuthenticated]: _id,
    }],
    nodes: [{}, PropTypes.object, {
      [actions.onCreate]: assignByK,
      [actions.onFind]: (s, r) => assignAllByK(s, r.find),
      [actions.onGet]: assignByK,
      [actions.onRemove]: omit,
      [actions.onVote]: assignByK,
    }],
  }),
})
