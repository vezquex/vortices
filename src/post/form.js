import {Helmet} from 'react-helmet'
import {checkbox, input} from '../util/react'
import state from './form-state'
const {Component, createElement: h} = require('react')

export default state(class extends Component {
	save = ev => {
		ev.preventDefault()
		const {k} = this.props
		const method = (k ? 'update' : 'create')
		// console.log('this', this)
		this.actions.apiSave(method, this.props.pre)
		.then(([e, r]) => r && this.props.success(k||r.k))
	}
	componentDidMount(){
		const {item, k} = this.props
		if(k && !item){
			this.actions.get({k})
		}
		this.actions.resetData({item, k})
	}
	render(){
		const {setTags, setData} = this.actions
		const {data, enabled, k, pre, tags, title} = this.props
		const {bump, content, href, name/*, svg*/} = data
		const {to} = pre || {}
		return (
			h('form', {
				className: 'main',
				onSubmit: this.save,
				},
				title && h(Helmet, null,
					h('title', null, title)
				),
				h('fieldset', {
					className: 'column expand',
					disabled:!enabled,
					},
					h('div', {
						className: 'fieldset-layout',
						},
						h('label', null,
							h('span', {className:'field-label'},
								'Title'
							),
							h('input', {value:name||'', onChange:input(setData, 'name'), autoFocus:true})
						),
						h('label', {className:'expand'},
							h('span', {className:'field-label'},
								'Content'
							),
							h('textarea', {
								className:'expand noresize',
								style:{minHeight:'6em'},
								onChange: input(setData, 'content'),
								value:content||'',
							})
						),
						// h('label', {className:'expand'},
						//   h('span', {className:'field-label'},
						//     'SVG'
						//   ),
						//   h('textarea', {
						//     className:'expand',
						//     // style:{minHeight:'6em'},
						//     onChange: input(setData, 'svg'),
						//     value:svg||'',
						//   })
						// ),
						h('label', null,
							h('span', {className:'field-label'},
								'Link'
							),
							h('input', {value:href||'', onChange:input(setData, 'href'), type:'url'})
						),
						h('label', null,
							h('span', {className:'field-label'},
								'Tags'
							),
							h('input', {value:tags||'', onChange:input(setTags, 'tags')})
						),
						to && h('label', {className:'holo-switch'},
							h('input', {checked:!!bump, onChange:checkbox(setData, 'bump'), type:'checkbox'}),
							h('div', {className:'field-label holo-label expand'},
								'Bump Thread'
							)
						),
						h('button', {className:'wide submit'}, k ? 'Save' : 'Post'),
					),
				)
			)
		)
	}
})
