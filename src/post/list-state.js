import _ from 'lodash'
import api from '../api'
import PropTypes from 'prop-types'
import state from '../state'
import {a_id, id, reject} from '../util/fun'
import {kea} from 'kea'

export default kea({
  // key: (props) => props.k,
  connect: {
    actions: [state, ['onFind', 'remove', 'vote']],
    props: [state, ['nodes']],
  },
  actions: () => ({
    clear: true,
    onListCreate: a_id,
    onListFind: a_id,
    onRemove: id,
    onVote: id,
  }),
  thunks: ({ actions }) => ({
    listFind: (key, o) => {
      actions.clear()
      return api.find(o).then(er => {
        actions.onFind(er)
        actions.onListFind(er)
      })
    },
    vote: (o) =>
      api.vote(o).then(actions.onVote),
  }),
  reducers: ({ actions }) => ({
    count: [0, PropTypes.number, {
      [actions.clear]: ()=>0,
      [actions.onListFind]: (_, r) => r.count || 0,
      [actions.onRemove]: c => c - 1,
    }],
    ks: [[], PropTypes.array, {
      [actions.clear]: ()=>[],
      [actions.onListCreate]: (s, r) => [r.k, ...s],
      [actions.onListFind]: (s, r) => _.map(r.find, 'k'),
      [actions.onRemove]: reject,
    }],
  }),
})