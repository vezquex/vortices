import Headline from './headline'
import state from './list-state'
import {Link} from 'react-router-dom'
import {Helmet} from 'react-helmet'
const {Component, createElement: h, createRef} = require('react')
const limit = 10

export default state(class extends Component {
  constructor(props){
    super(props)
    this.ref = createRef()
  }
  componentDidMount = () => {
    const {find, offset} = this.props
    find(this.find, {offset})
  }
  componentDidUpdate = (prevProps) => {
    const {find, list, offset} = this.props
    if(
      offset !== prevProps.offset
      || list !== prevProps.list
    ){
      find(this.find, {offset})
      .then(()=>this.ref.current.scrollTo(0, 0))
    }
  }
  find = (o) => (
    this.actions.listFind(this.props.list, o)
  )
  render(){
    const {props, ref} = this
    const {className, compact, content, count, hide, ks, nodes, offset, tags, title} = props
    const length = ks ? ks.length : 0
    const back = offset - limit
    const more = count - offset - length
    const component = props.component || Headline
    const root = (className === 'main') ? 'main' : 'div'
    return (
      h(root, {ref, className:'column '+className},
        title && h(Helmet, null,
          h('title', null, title)
        ),
        (offset > 0) && h(Link, {
          to: (back > 0) ? '?offset='+(back) : '?',
          className:'tab',
          },
          'Back'
        ),
        ks && ks.map(k =>
          h(component, {compact, content, hide, item: nodes[k], key:k, tags})
        ),
        (more > 0) && h(Link, {
          to:'?offset='+(offset + length),
          className:'tab expand unvisited',
          },
          more+' More'
        ),
      )
    )
  }
})
