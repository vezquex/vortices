import _ from 'lodash'
import appState from '../state'
import now from '../util/now'
import {kea} from 'kea'
import {Link} from 'react-router-dom'
const {Component, createElement: h} = require('react')
const {key} = require('../util/auth')
const www = /^www./

const state = kea({
	connect: {
		actions: [appState, ['remove', 'vote']],
		props: [appState, ['auth']],
	},
})

export default state(class extends Component {
	remove = () => {
		const d = window.confirm('Delete?')
		return d && this.actions.remove(this.props.item.k)
	}
	render(){
		const {actions, props, remove} = this
		const {auth, compact, item} = props
		if(!item) return null
		const hide = props.hide || {}
		const show = props.show || {}
		const u = auth.k
		const disabled = !u
		const enabled = !disabled
		const {actor, content, down, href, k, name, published, replies, svg, up} = item
		let url
		try {
			url = href && new URL(href)
		} catch(e){}
		const domain = url && url.hostname.replace(www, '')
		const match = enabled && actor && (u === key(actor))
		const vote = item.vote || {}
		const node = '/node/' + k
		const tags = props.tags ? _.difference(item.tags, props.tags) : item.tags
		const [date, time] = now(published).split('T')
		const time_short = time.substring(0,5)
		return h('div', { className: 'stripe' },
			h('div', {
				className: [
					'headline',
					(compact && 'compact'),
				].join(' '),
			},
			h((show.link_self && href) ? 'a' : Link, {
				className: 'url',
				href,
				to: node,
			},
				(name || !compact) && h('span', { className: 'title' },
					name || '\u00A0'
				),
				h('span', { className: 'info' },
					!hide.replies && (replies > 0) && h('span', null, `${replies} ${replies === 1 ? 'reply' : 'replies'}`),
					domain && h('span', null,
						domain,
						// h('img', {src: url.origin + '/favicon.ico', className:'favicon', alt:'', width:'16px'}),
					),
					!hide.actor && h('span', null, actor),
					h('span', null, `${date} ${time_short}`),
					!hide.tags && tags && h('span', null, Array.from(tags).join(' '))
				)
			),
			svg && h(Link, {
				to: node,
				},
				h('div', {className:'svg', dangerouslySetInnerHTML:{__html:svg}}),
			),
			match && h('button', {
				className:'headline-button',
				onClick: remove,
				title: 'Delete',
				},
				h('span', null, '\u00A0'),
				h('span', null, 'Del')
			),
			href && h('a',
				{
					className: 'to',
					href,
				},
				h('span', {className:'count'},
					'↗',
				),
				h('span', null,	'Go'),
			),
			show.re && h(Link, {
				className: 'to',
				title: 'Replies',
				to: node,
				},
				h('span', {className:'count'},
					replies || '0',
				),
				h('span', null,	'Re')
			),
			h('button', {
				disabled,
				onClick: (()=>actions.vote({down:0, up:vote.up ? 0 : 1, k})),
				},
				h('span', {className:'count'}, !!vote.up && '/', up||0),
				h('span', null, 'Up')
			),
			h('button', {
				disabled,
				onClick: (()=>actions.vote({down:vote.down ? 0 : 1, up:0, k})),
			},
				h('span', {className:'count'}, !!vote.down && '/', down||0),
				h('span', null, 'Down')
			),
		),
		content && props.content && h('p', {className:'content wide'}, content),
	)
	}
})