import {_id, assign, id} from '../util/fun'
import {kea} from 'kea'
import api from '../api'
import appState, {handleError} from '../state'
import listState from './list-state'
import PropTypes from 'prop-types'

const blank = {
  bump: true,
  content: '',
  href: '',
  name: '',
  svg: '',
}

const toStr = x => [].concat(x).join(', ')
const setTags = (_, {tags}) => toStr(tags)

export default kea({
  connect: {
    actions: [
      appState, ['get', 'onAuthenticated', 'onCreate', 'onGet'],
      listState, ['onListCreate'],
    ],
  },
  actions: () => ({
    clear: true,
    resetData: id,
    setData: id,
    setTags: o => ({tags: toStr(o.tags)}),
  }),
  thunks: ({ actions, get, getState }) => ({
    apiSave: (method, pre) => {
      const data = get('data')
      const {t} = getState().scenes.state.auth
      const tags = get('tags')
      const {to} = pre || {}
      const post = api[method]({...data, t, tags, to})
      post.then(handleError(actions, r => {
        actions.clear()
        actions.onCreate(r)
        actions.onListCreate(r)
      }))
      return post
    },
    // get: o => api.get(o).then(actions.onGet),
  }),
  reducers: (ro) => {
    const { actions/*, props*/ } = ro
    return {
      data: [
        blank,
        PropTypes.object,
        {
          [actions.clear]: ()=>blank,
          [actions.onGet]: _id,
          [actions.resetData]: (_, {item, k}) =>
            (item && (item.k === k)) ? item : blank,
          [actions.setData]: assign,
          [actions.setTags]: assign,
        }
      ],
      tags: [
        '',
        PropTypes.string,
        {
          [actions.clear]: ()=>'',
          [actions.onGet]: setTags,
          [actions.resetData]: (_, {item, k}) =>
            (item && (item.k === k)) ? toStr(item.tags) : '',
          [actions.setTags]: setTags,
        }
      ],
      enabled: [true],
    }
  }
})