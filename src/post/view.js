import appState from '../state'
import Form from './form'
import Headline from './headline'
import List from './list'
import {Helmet} from 'react-helmet'
import {kea} from 'kea'
import {Link} from 'react-router-dom'
const {Component, createElement: h} = require('react')
const {key} = require('../util/auth')

const state = kea({
  connect: {
    actions: [appState, ['get']],
    props: [appState, ['auth']],
  },
})

export default state(class extends Component {
	state = {
		r: false,
	}
  componentDidMount(){
		const {k} = this.props
		this.actions.get({k})
	}
  componentDidUpdate = (prevProps) => {
    const {k} = this.props
    if(k !== prevProps.k){
      this.actions.get({k})
    }
  }
	toggleReply = () => {
		this.setState(({r}) => ({r:!r}))
	}
	render(){
		const {props, state} = this
		const {auth, getError, find, isAuth, item, k, offset} = props
		const {r} = state
		if(getError || !item){
			return h('p', {}, getError && ('No item.'))
		}
		const {actor, content, name, replies, svg, tags, to} = item
		const u = auth.k
		const match = (u === key(actor))
		return h('div', { className: 'main' },
			h(Helmet, null,
				h('title', null, name || (content && content.slice(0,Math.min(32, content.indexOf('\n')))))
			),
			h('div', {className:'menu column'},
				to && h(Link, {to:'/node/'+to}, 'Up'),
			),
			h(Headline, {hide:{actor:true, replies:true, tags:true}, show:{link_self:true}, item}),
			tags && !!tags.length && h('div', {className:'menu row sys'},
				h('div', {className:'tab'}, 'Tags'),
				tags.map(tag => h(Link, {to:'/tag/'+tag, key:tag}, tag))
			),
			// tags && !!tags.length && h('div', {className:'menu row sys'},
			// 	h('div', {className:'tab'}, 'Views'),
			// 	tags.map(tag => h(Link, {to:`/t/${tag}/`+k, key:tag}, tag))
			// ),
			actor && h('div', {className:'menu row sys'},
				h('div', {className:'tab'}, 'By'),
				h(Link, {to:'/u/'+actor}, actor)
			),
			match && h(Link, {
				to:'/edit/'+k,
				className:'tab expand',
			}, 'Edit'),
			svg && h('div', {className:'svg', dangerouslySetInnerHTML:{__html:svg}}),
			h('p', {className:'content'}, content),
			isAuth && h('div', {className:'menu column'},
				h('button', {onClick:this.toggleReply}, 'Reply')
			),
			r && h(Form, {
				pre: {to:k},
				success: this.toggleReply,
			}),
			(replies > 0) && h('p', {className:'sys'}, `${replies} ${replies === 1 ? 'reply' : 'replies'}`),
			h(List, {
				compact:true,
				content:true,
				find,
				list:k,
				offset,
			})
		)
	}
})